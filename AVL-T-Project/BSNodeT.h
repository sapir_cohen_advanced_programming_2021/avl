#pragma once

#include <string>
#include <iostream>

#define ERROR_RET -1

template <class T>
class BSNode
{
public:

	BSNode()
	{
		this->_data = "";
		this->_right = this->_left = nullptr;
		this->_count = 0;
	}

	BSNode(T data);
	BSNode(const BSNode& other);

	virtual ~BSNode();

	virtual BSNode* insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const;

protected:
	T _data;
	BSNode* _left;
	BSNode* _right;
	int _count;
};

template <class T>
BSNode<T>::BSNode(T data)
{
	this->_data = data;
	this->_right = this->_left = nullptr;
	this->_count = 1;
}

template <class T>
BSNode<T>::BSNode(const BSNode& other)
{
	this->_data = other.getData();
	this->_count = other._count;
	if (other.getRight() != nullptr) this->_right = new BSNode(*other.getRight());
	else this->_right = nullptr;
	if (other.getLeft() != nullptr) this->_left = new BSNode(*other.getLeft());
	else this->_left = nullptr;
}

template <class T>
BSNode<T>::~BSNode()
{
	if (this->_right != nullptr) this->_right->~BSNode();
	else delete this->_right;
	if (this->_left != nullptr) this->_left->~BSNode();
	else delete this->_left;
}

template <class T>
BSNode<T>* BSNode<T>::insert(T value)
{
	if (this->_data == value)
	{
		this->_count++;
	}
	else if (this->_data < value)
	{
		if (this->_right != nullptr)
		{
			this->_right->insert(value);
			return this;
		}
		else this->_right = new BSNode(value);
	}
	else
	{
		if (this->_left != nullptr)
		{
			this->_left->insert(value);
			return this;
		}
		else this->_left = new BSNode(value);
	}
	return this;
}

template <class T>
BSNode<T>& BSNode<T>::operator=(const BSNode& other)
{
	*this = BSNode(other);
	return *this;
}

template <class T>
bool BSNode<T>::isLeaf() const
{
	return this->_right == nullptr && this->_left == nullptr;
}

template <class T>
T BSNode<T>::getData() const
{
	return this->_data;
}

template <class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return this->_left;
}

template <class T>
BSNode<T>* BSNode<T>::getRight() const
{
	return this->_right;
}

template <class T>
bool BSNode<T>::search(T val) const
{
	if (this->_data == val) return true;
	else if (this->_data < val && this->_right != nullptr) return this->_right->search(val);
	else if (this->_left != nullptr) return this->_left->search(val);
	return false;
}

template <class T>
int BSNode<T>::getHeight() const
{
	if (this == nullptr || this->isLeaf()) return 0;
	else
	{
		if (this->_right->getHeight() + 1 > this->_left->getHeight() + 1) return this->_right->getHeight() + 1;
		else return this->_left->getHeight() + 1;
	}
}

template <class T>
int BSNode<T>::getDepth(const BSNode<T>& root) const
{
	if (root.search(this->_data))
	{
		if (this->_data == root.getData()) return 0;
		else if (root.getData() < this->_data) return this->getDepth(*root.getRight()) + 1;
		else return this->getDepth(*root.getLeft()) + 1;
	}
	else return ERROR_RET;
}

template <class T>
void BSNode<T>::printNodes() const
{
	if (this != nullptr)
	{
		if (this->_left != nullptr) this->_left->printNodes();
		std::cout << this->_count << " " << this->_data << std::endl;
		if (this->_right != nullptr) this->_right->printNodes();
	}
}
