#include "AVLTree.h"
#include "printTreeToFile.h"
#include <fstream>
#include <iostream>
#include <string>
#include <windows.h>

int main() {

	BSNode<double> *avl = new AVLTree<double>(5);

	try {
		// checks the different rotations
		
		BSNode<double> *avl1 = new AVLTree<double>(1);
		avl1 = avl1->insert(3);
		avl1 = avl1->insert(2);
		
		BSNode<double> *avl2 = new AVLTree<double>(1);
		avl2 = avl2->insert(2);
		avl2 = avl2->insert(3);
		
		BSNode<double> *avl3 = new AVLTree<double>(3);
		avl3 = avl3->insert(2);
		avl3 = avl3->insert(1);

		BSNode<double> *avl4 = new AVLTree<double>(3);
		avl4 = avl4->insert(1);
		avl4 = avl4->insert(2);
		
		std::string textTree = "BSTData.txt";
		avl = avl->insert(6);
		avl = avl->insert(4);
		avl = avl->insert(7);
		avl = avl->insert(6.5);
		avl = avl->insert(4.5);
		avl = avl->insert(1);
		avl = avl->insert(32);
		avl = avl->insert(23);
		char a[] = { "a" };
		
		printTreeToFile(avl, textTree);
		system("BinaryTree.exe");
		system("pause");

	}
	catch (std::invalid_argument &e) {
		std::cout << "EXCEPTION: " << e.what() << std::endl;
		system("pause");
	}

	delete avl;
	return 0;
}
