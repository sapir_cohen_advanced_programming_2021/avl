#pragma once
#include <iostream>
#include "BSNodeT.h"

enum unbalanced { RR_T_RL_T = -2, LR_L_RR_R, NORM, LL_L_RL_R, LL_T_LR_T };

template <class T>
class AVLTree : public BSNode<T>
{
public:

	AVLTree(T data) : BSNode<T>(data) {}

	virtual AVLTree<T>* insert(T value);

private:
	int getBalanceFactor();

	AVLTree<T>* rotateRight();
	AVLTree<T>* rotateLeft();

	AVLTree<T>* balance(); /* Checking Rotations */

};

template <class T>
int AVLTree<T>::getBalanceFactor()
{
	int hR = 0, hL = 0;
	if (this->_right) hR = this->_right->getHeight() + 1;
	if (this->_left) hL = this->_left->getHeight() + 1;
	return hL - hR;
}

template <class T>
AVLTree<T>* AVLTree<T>::rotateRight()
{
	if (this->_left != nullptr)
	{
		AVLTree* temp = (AVLTree<T>*) this->_left;
		this->_left = this->_left->getRight();
		temp->_right = this;
		return temp;
	}
	return this;
}

template <class T>
AVLTree<T>* AVLTree<T>::rotateLeft()
{
	if (this->_right != nullptr)
	{
		AVLTree* temp = (AVLTree<T>*) this->_right;
		this->_right = this->_right->getLeft();
		temp->_left = this;
		return temp;
	}
	return this;
}

template <class T>
AVLTree<T>* AVLTree<T>::insert(T value)
{
	if (this->_data == value)
	{
		throw std::invalid_argument("Already inserted this value");
	}
	else if (this->_data < value)
	{
		if (this->_right != nullptr) this->_right = this->_right->insert(value);
		else this->_right = new AVLTree(value);
	}
	else
	{
		if (this->_left != nullptr) this->_left = this->_left->insert(value);
		else this->_left = new AVLTree(value);
	}
	return this->balance();
}

template <class T>
AVLTree<T>* AVLTree<T>::balance()
{
	if (this->_right != nullptr)
	{
		AVLTree<T>* rightChild = (AVLTree<T>*) this->_right;
		if (this->getBalanceFactor() == RR_T_RL_T && rightChild->getBalanceFactor() == LR_L_RR_R) //RR
		{
			return rotateLeft();
		}
		else if (this->getBalanceFactor() == RR_T_RL_T && rightChild->getBalanceFactor() == LL_L_RL_R) //RL
		{
			rightChild = rightChild->rotateRight();
			this->_right = rightChild;
			return this->rotateLeft();
		}
	}
	else if (this->_left != nullptr)
	{
		AVLTree<T>* leftChild = (AVLTree<T>*) this->_left;
		if (this->getBalanceFactor() == LL_T_LR_T && leftChild->getBalanceFactor() == LL_L_RL_R) //LL
		{
			return rotateRight();
		}
		else if (this->getBalanceFactor() == LL_T_LR_T && leftChild->getBalanceFactor() == LR_L_RR_R) //LR
		{
			leftChild = leftChild->rotateLeft();
			this->_left = leftChild;
			return this->rotateRight();
		}
	}
	return this;
}