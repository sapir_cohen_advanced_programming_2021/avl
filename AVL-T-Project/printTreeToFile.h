#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include "BSNodeT.h"
#include "AVLTree.h"

template <class T>
void printTreeToFile(const BSNode<T>* bs, std::string output)
{
	std::ofstream file(output);
	std::string data = "";
	if (file.is_open())
	{
		getBSData(bs, data);
		file << data;
		file.close();
	}
}

//A postorder of bs data
template <class T>
void getBSData(const BSNode<T>* bs, std::string& data)
{
	if (bs != nullptr)
	{
		data += std::to_string(bs->getData()) + " ";
		if (bs->getLeft() != nullptr) getBSData(bs->getLeft(), data);
		else data += "# ";
		if (bs->getRight() != nullptr) getBSData(bs->getRight(), data);
		else data += "# ";
	}
}
